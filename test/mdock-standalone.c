/*
 * Copyright 2015 Javier S. Pedro <dev.git@javispedro.com>
 *
 * This file is part of MDock.
 *
 * MDock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MDock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MDock.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../config.h"
#include <stdlib.h>
#include <locale.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include "../libmdock/mdock-widget.h"

static GtkWindow *mainwin;
static MDockWidget *mdock;

static void construct_main_window()
{
	mainwin = GTK_WINDOW(gtk_window_new(GTK_WINDOW_TOPLEVEL));
	mdock = MDOCK_WIDGET(mdock_widget_new_with_settings_path("/com/javispedro/mdock/standalone/"));
	gtk_orientable_set_orientation(GTK_ORIENTABLE(mdock), GTK_ORIENTATION_HORIZONTAL);
	gtk_container_add(GTK_CONTAINER(mainwin), GTK_WIDGET(mdock));
	gtk_window_set_title(mainwin, "MDock standalone");
}

int main(int argc, char **argv)
{
	setlocale(LC_ALL, "");
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    textdomain(GETTEXT_PACKAGE);

	gtk_init(&argc, &argv);

	construct_main_window();

	g_signal_connect(mainwin, "destroy", G_CALLBACK(gtk_main_quit), NULL);

	gtk_window_set_keep_above(mainwin, TRUE);
	gtk_window_set_skip_taskbar_hint(mainwin, TRUE);
	gtk_window_set_accept_focus(mainwin, FALSE);
	gtk_widget_show_all(GTK_WIDGET(mainwin));

	gtk_main();

	return EXIT_SUCCESS;
}
