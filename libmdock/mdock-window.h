#ifndef __MDOCK_WINDOW_H__
#define __MDOCK_WINDOW_H__

#include <gtk/gtk.h>
#define WNCK_I_KNOW_THIS_IS_UNSTABLE 1
#include <libwnck/libwnck.h>

G_BEGIN_DECLS

#define MDOCK_TYPE_WINDOW				(mdock_window_get_type ())
#define MDOCK_WINDOW(obj)				(G_TYPE_CHECK_INSTANCE_CAST ((obj), MDOCK_TYPE_WINDOW, MDockWindow))
#define MDOCK_WINDOW_CONST(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), MDOCK_TYPE_WINDOW, MDockWindow const))
#define MDOCK_WINDOW_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), MDOCK_TYPE_WINDOW, MDockWindowClass))
#define MDOCK_IS_WINDOW(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), MDOCK_TYPE_WINDOW))
#define MDOCK_IS_WINDOW_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), MDOCK_TYPE_WINDOW))
#define MDOCK_WINDOW_GET_CLASS(obj)		(G_TYPE_INSTANCE_GET_CLASS ((obj), MDOCK_TYPE_WINDOW, MDockWindowClass))

typedef struct _MDockWindow			MDockWindow;
typedef struct _MDockWindowClass	MDockWindowClass;
typedef struct _MDockWindowPrivate	MDockWindowPrivate;

struct _MDockWindow {
	GtkEventBox parent;
	WnckWindow *window;
	MDockWindowPrivate *priv;
};

struct _MDockWindowClass {
	GtkEventBoxClass parent_class;
};

GType mdock_window_get_type(void) G_GNUC_CONST;

void mdock_window_update_screenshot(MDockWindow *window);
void mdock_window_schedule_screenshot(MDockWindow *window);

MDockWindow *mdock_window_new(WnckWindow *window);


G_END_DECLS

#endif /* __MDOCK_WINDOW_H__ */
