/*
 * Copyright 2015 Javier S. Pedro <dev.git@javispedro.com>
 *
 * This file is part of MDock.
 *
 * MDock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MDock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MDock.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MDOCK_ITEM_MENU_H__
#define __MDOCK_ITEM_MENU_H__

#include "mdock-item.h"

G_BEGIN_DECLS

#define MDOCK_TYPE_ITEM_MENU			(mdock_item_menu_get_type ())
#define MDOCK_ITEM_MENU(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), MDOCK_TYPE_ITEM_MENU, MDockItemMenu))
#define MDOCK_ITEM_MENU_CONST(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), MDOCK_TYPE_ITEM_MENU, MDockItemMenu const))
#define MDOCK_ITEM_MENU_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), MDOCK_TYPE_ITEM_MENU, MDockItemMenuClass))
#define MDOCK_IS_ITEM_MENU(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), MDOCK_TYPE_ITEM_MENU))
#define MDOCK_IS_ITEM_MENU_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), MDOCK_TYPE_ITEM_MENU))
#define MDOCK_ITEM_MENU_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), MDOCK_TYPE_ITEM_MENU, MDockItemMenuClass))

typedef struct _MDockItemMenu			MDockItemMenu;
typedef struct _MDockItemMenuClass		MDockItemMenuClass;
typedef struct _MDockItemMenuPrivate	MDockItemMenuPrivate;

struct _MDockItemMenu {
	GtkMenu parent;
	MDockItem *item;
	MDockItemMenuPrivate *priv;
};

struct _MDockItemMenuClass {
	GtkMenuClass parent_class;
};

GType mdock_item_menu_get_type(void) G_GNUC_CONST;
MDockItemMenu *mdock_item_menu_new(MDockItem *item);

G_END_DECLS

#endif /* __MDOCK_ITEM_MENU_H__ */
