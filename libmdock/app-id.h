/*
 * Copyright 2015 Javier S. Pedro <dev.git@javispedro.com>
 *
 * This file is part of MDock.
 *
 * MDock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MDock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MDock.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MDOCK_APPID_H_
#define _MDOCK_APPID_H_

#include <glib.h>
#define WNCK_I_KNOW_THIS_IS_UNSTABLE 1
#include <libwnck/libwnck.h>

typedef struct _AppId {
	gchar *host;
	gint uid;
	gchar *executable;
	gchar *wm_class_class;
	gchar *wm_class_name;
} AppId;

AppId * app_id_new();
AppId * app_id_create(const gchar *host, gint uid, const gchar *executable, const gchar *wm_class_class, const gchar *wm_class_name);
void app_id_destroy(AppId *appid);

guint app_id_hash(gconstpointer appid);
gboolean app_id_equal(gconstpointer ap, gconstpointer bp);

AppId * app_id_from_window(WnckWindow *window);

gboolean app_id_is_local_user(AppId *appid);

#endif /* _MDOCK_APPID_H_ */
