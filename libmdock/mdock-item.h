/*
 * Copyright 2015 Javier S. Pedro <dev.git@javispedro.com>
 *
 * This file is part of MDock.
 *
 * MDock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MDock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MDock.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MDOCK_ITEM_H__
#define __MDOCK_ITEM_H__

#include <gtk/gtk.h>
#include <gio/gdesktopappinfo.h>
#define WNCK_I_KNOW_THIS_IS_UNSTABLE 1
#include <libwnck/libwnck.h>

G_BEGIN_DECLS

#define MDOCK_TYPE_ITEM             (mdock_item_get_type())
#define MDOCK_ITEM(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), MDOCK_TYPE_ITEM, MDockItem))
#define MDOCK_ITEM_CONST(obj)       (G_TYPE_CHECK_INSTANCE_CAST ((obj), MDOCK_TYPE_ITEM, MDockItem const))
#define MDOCK_ITEM_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), MDOCK_TYPE_ITEM, MDockItemClass))
#define MDOCK_IS_ITEM(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MDOCK_TYPE_ITEM))
#define MDOCK_IS_ITEM_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), MDOCK_TYPE_ITEM))
#define MDOCK_ITEM_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), MDOCK_TYPE_ITEM, MDockItemClass))

typedef struct _MDockItem			MDockItem;
typedef struct _MDockItemClass		MDockItemClass;
typedef struct _MDockItemPrivate	MDockItemPrivate;

struct _MDockItem {
	GtkWidget parent;

	MDockItemPrivate *priv;
};

struct _MDockItemClass {
	GtkWidgetClass parent_class;
};

GType mdock_item_get_type(void) G_GNUC_CONST;
MDockItem *mdock_item_new();

gboolean mdock_item_get_pinned(MDockItem *self);
void mdock_item_set_pinned(MDockItem *self, gboolean pinned);
void mdock_item_toggle_pinned(MDockItem *self);

GDesktopAppInfo *mdock_item_get_desktop_app_info(MDockItem *self);
void mdock_item_set_desktop_app_info(MDockItem *self, GDesktopAppInfo *app_info);
void mdock_item_launch_files(MDockItem *self, GList *files);
void mdock_item_launch_file(MDockItem *self, GFile *file);
void mdock_item_launch(MDockItem *self);

const gchar *mdock_item_get_display_name(MDockItem *self);
GdkPixbuf *mdock_item_get_icon_pixbuf(MDockItem *self);
gboolean mdock_item_needs_attention(MDockItem *self);

void mdock_item_add_window(MDockItem *self, WnckWindow *window);
void mdock_item_remove_window(MDockItem *self, WnckWindow *window);
void mdock_item_set_last_active_window(MDockItem *self, WnckWindow *window);
gint mdock_item_get_num_windows(MDockItem *self);
gboolean mdock_item_has_windows(MDockItem *self);

void mdock_item_close_all_windows(MDockItem *self);

void mdock_item_update_background(MDockItem *self);

void mdock_item_activate(MDockItem *self);

G_END_DECLS

#endif /* __MDOCK_ITEM_H__ */
