/*
 * Copyright 2015 Javier S. Pedro <dev.git@javispedro.com>
 *
 * This file is part of MDock.
 *
 * MDock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MDock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MDock.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MDOCK_WIDGET_H_
#define _MDOCK_WIDGET_H_

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define MDOCK_TYPE_WIDGET           mdock_widget_get_type()
#define MDOCK_WIDGET(obj)           (G_TYPE_CHECK_INSTANCE_CAST((obj), MDOCK_TYPE_WIDGET, MDockWidget))
#define MDOCK_IS_WIDGET(obj)        (G_TYPE_CHECK_INSTANCE_TYPE((obj), MDOCK_TYPE_WIDGET))
#define MDOCK_WIDGET_CLASS(c)       (G_TYPE_CHECK_CLASS_CAST((c), MDOCK_TYPE_WIDGET, MDockWidgetClass))
#define MDOCK_IS_WIDGET_CLASS(c)    (G_TYPE_CHECK_CLASS_TYPE((c), MDOCK_TYPE_WIDGET))
#define MDOCK_WIDGET_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), MDOCK_TYPE_WIDGET, MDockWidgetClass))

#define MDOCK_WIDGET_GSETTINGS_SCHEMA "com.javispedro.mdock.widget"

typedef struct _MDockWidget        MDockWidget;
typedef struct _MDockWidgetClass   MDockWidgetClass;
typedef struct _MDockWidgetPrivate MDockWidgetPrivate;

struct _MDockWidget
{
	GtkBox parent_instance;

	MDockWidgetPrivate *priv;
};

struct _MDockWidgetClass
{
	GtkBoxClass parent_class;
};

GType mdock_widget_get_type(void);

gint mdock_widget_get_n_items(MDockWidget *widget);

void mdock_widget_update_background(MDockWidget *widget);

GtkWidget *mdock_widget_new(void);
GtkWidget *mdock_widget_new_with_settings(GSettings *settings);
GtkWidget *mdock_widget_new_with_settings_path(const gchar *path);

G_END_DECLS

#endif /* _MDOCK_WIDGET_H_ */
