/*
 * Copyright 2015 Javier S. Pedro <dev.git@javispedro.com>
 *
 * This file is part of MDock.
 *
 * MDock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MDock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MDock.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MDOCK_MATE_PANEL_APPLET_H_
#define _MDOCK_MATE_PANEL_APPLET_H_

#include <mate-panel-applet.h>
#include "../libmdock/mdock-widget.h"

G_BEGIN_DECLS

#define MDOCK_TYPE_MATE_PANEL_APPLET           mdock_mate_panel_applet_get_type()
#define MDOCK_MATE_PANEL_APPLET(obj)           (G_TYPE_CHECK_INSTANCE_CAST((obj), MDOCK_TYPE_MATE_PANEL_APPLET, MDockMatePanelApplet))
#define MDOCK_IS_MATE_PANEL_APPLET(obj)        (G_TYPE_CHECK_INSTANCE_TYPE((obj), MDOCK_TYPE_MATE_PANEL_APPLET))
#define MDOCK_MATE_PANEL_APPLET_CLASS(c)       (G_TYPE_CHECK_CLASS_CAST((c), MDOCK_TYPE_MATE_PANEL_APPLET, MDockMatePanelAppletClass))
#define MDOCK_IS_MATE_PANEL_APPLET_CLASS(c)    (G_TYPE_CHECK_CLASS_TYPE((c), MDOCK_TYPE_MATE_PANEL_APPLET))
#define MDOCK_MATE_PANEL_APPLET_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), MDOCK_TYPE_MATE_PANEL_APPLET, MDockMatePanelAppletClass))

typedef struct _MDockMatePanelApplet        MDockMatePanelApplet;
typedef struct _MDockMatePanelAppletClass   MDockMatePanelAppletClass;

struct _MDockMatePanelApplet
{
	MatePanelApplet parent_instance;
	MDockWidget *dock;
};

struct _MDockMatePanelAppletClass
{
	MatePanelAppletClass parent_class;
};

GType mdock_mate_panel_applet_get_type(void);

MatePanelApplet *mdock_mate_panel_applet_new(void);

G_END_DECLS

#endif /* _MDOCK_MATE_PANEL_APPLET_H_ */
