/*
 * Copyright 2015 Javier S. Pedro <dev.git@javispedro.com>
 *
 * This file is part of MDock.
 *
 * MDock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MDock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MDock.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../config.h"

#include <glib/gi18n-lib.h>
#include <mate-panel-applet-gsettings.h>

#include "mdock-mate-panel-applet.h"

G_DEFINE_TYPE(MDockMatePanelApplet, mdock_mate_panel_applet, PANEL_TYPE_APPLET)

static void display_preferences_dialog(GtkAction *action, MDockMatePanelApplet *self)
{
	// TODO
}

static void display_about_dialog(GtkAction *action, MDockMatePanelApplet *self)
{
	GtkWindow *parent = NULL;
	GtkWidget *parent_widget = gtk_widget_get_toplevel(GTK_WIDGET(self));
	if (GTK_IS_WINDOW(parent_widget)) {
		parent = GTK_WINDOW(parent_widget);
	}

	gtk_show_about_dialog(parent,
	                      "program-name", "MDock Mate Panel Applet",
	                      NULL);
}

static const GtkActionEntry menu_verbs[] = {
	{ "MDockPreferences", GTK_STOCK_PROPERTIES, N_("_Preferences"),
	  NULL, NULL,
	  G_CALLBACK (display_preferences_dialog) },
	{ "MDockAbout", GTK_STOCK_ABOUT, N_("_About"),
	  NULL, NULL,
	  G_CALLBACK (display_about_dialog) }
};

static inline GtkOrientation applet_orient_to_orientation(MatePanelAppletOrient orient)
{
	switch (orient) {
	case MATE_PANEL_APPLET_ORIENT_DOWN:
	case MATE_PANEL_APPLET_ORIENT_UP:
		return GTK_ORIENTATION_HORIZONTAL;
	case MATE_PANEL_APPLET_ORIENT_LEFT:
	case MATE_PANEL_APPLET_ORIENT_RIGHT:
		return GTK_ORIENTATION_VERTICAL;
	default:
		g_return_val_if_reached(GTK_ORIENTATION_HORIZONTAL);
	}
}

static gboolean transform_applet_orient_to_orientation(GBinding *binding, const GValue *from_value, GValue *to_value, gpointer user_data)
{
	MatePanelAppletOrient orient = g_value_get_uint(from_value);
	g_value_set_enum(to_value, applet_orient_to_orientation(orient));
	return TRUE;
}

static gboolean transform_prefs_path_to_gsettings(GBinding *binding, const GValue *from_value, GValue *to_value, gpointer user_data)
{
	const gchar *prefs_path = g_value_get_string(from_value);
	GSettings *settings = g_settings_new_with_path(MDOCK_WIDGET_GSETTINGS_SCHEMA, prefs_path);
	g_return_val_if_fail(settings, FALSE);
	g_value_take_object(to_value, settings);
	return TRUE;
}

static GtkOrientation mdock_mate_panel_applet_get_orientation(MDockMatePanelApplet *self)
{
	MatePanelAppletOrient orient = mate_panel_applet_get_orient(MATE_PANEL_APPLET(self));
	return applet_orient_to_orientation(orient);
}

static void mdock_mate_panel_applet_size_allocate(GtkWidget *widget, GtkAllocation *allocation)
{
	MDockMatePanelApplet *self = MDOCK_MATE_PANEL_APPLET(widget);
	GTK_WIDGET_CLASS(mdock_mate_panel_applet_parent_class)->size_allocate(widget, allocation);
	gtk_widget_size_allocate(GTK_WIDGET(self->dock), allocation);
}

static void mdock_mate_panel_applet_size_request(GtkWidget *widget, GtkRequisition *requisition)
{
	MDockMatePanelApplet *self = MDOCK_MATE_PANEL_APPLET(widget);
	gint nitems = mdock_widget_get_n_items(self->dock);

	gtk_widget_size_request(GTK_WIDGET(self->dock), requisition);

	// Try to force a square aspect ratio for the icons
	// by requesting a big enough major panel dimension
	GtkAllocation allocation;
	gtk_widget_get_allocation(GTK_WIDGET(self), &allocation);
	gint spacing = gtk_box_get_spacing(GTK_BOX(self->dock));

	if (mdock_mate_panel_applet_get_orientation(self) == GTK_ORIENTATION_HORIZONTAL) {
		requisition->width = MAX(requisition->width, nitems * (allocation.height + spacing));
		requisition->height = MAX(requisition->height, allocation.height);
	} else {
		requisition->width = MAX(requisition->width, allocation.width);
		requisition->height = MAX(requisition->height, nitems * (requisition->width + spacing));
	}
}

static void mdock_mate_panel_applet_class_init(MDockMatePanelAppletClass *klass)
{
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);
	widget_class->size_allocate = mdock_mate_panel_applet_size_allocate;
	widget_class->size_request = mdock_mate_panel_applet_size_request;

	// Let's do some stuff to initialize the process..
	// A decent default directory for spawned processes.
	if (chdir(g_get_home_dir()) != 0) {
		g_message("Failed to set cwd to the home directory");
	}
}

static void mdock_mate_panel_applet_init(MDockMatePanelApplet *self)
{
	MatePanelApplet *applet = MATE_PANEL_APPLET(self);

	self->dock = MDOCK_WIDGET(mdock_widget_new());
	gtk_container_add(GTK_CONTAINER(self), GTK_WIDGET(self->dock));

	g_object_bind_property_full(self, "orient", self->dock, "orientation",
	                            G_BINDING_DEFAULT,
	                            transform_applet_orient_to_orientation,
	                            NULL, NULL, NULL);
	g_object_bind_property_full(self, "prefs-path", self->dock, "settings",
	                            G_BINDING_DEFAULT,
	                            transform_prefs_path_to_gsettings,
	                            NULL, NULL, NULL);
	g_signal_connect_object(self, "change-background",
	                        G_CALLBACK(mdock_widget_update_background), self->dock,
	                        G_CONNECT_SWAPPED);

	GtkActionGroup *action_group = gtk_action_group_new("MDock Mate Panel Applet Actions");
	gtk_action_group_add_actions(action_group,
	                             menu_verbs, G_N_ELEMENTS(menu_verbs), self);

	mate_panel_applet_set_flags(applet,
	                            MATE_PANEL_APPLET_EXPAND_MINOR | MATE_PANEL_APPLET_HAS_HANDLE);
	mate_panel_applet_setup_menu(applet,
	                             "<menuitem name=\"MDock Preferences Item\" action=\"MDockPreferences\"/>"
	                             "<menuitem name=\"MDock About Item\" action=\"MDockAbout\"/>",
	                             action_group);
	mate_panel_applet_set_background_widget(applet, GTK_WIDGET(self));

	g_object_unref(action_group);

	gtk_widget_show(GTK_WIDGET(self->dock));
}

MatePanelApplet *mdock_mate_panel_applet_new(void)
{
	return MATE_PANEL_APPLET(g_object_new(MDOCK_TYPE_MATE_PANEL_APPLET, NULL));
}
